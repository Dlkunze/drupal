<?php

/**
 * Gamit theme stuff
 */
 
// if the right column region is populated, add a class - 'right-callout' - to the classes array

function gambit_preprocess_html(&$variables){
    if(!empty($variables['page']['right_callout'])){
        $variables['classes_array'][] = "right-callout";
    }    
} 

function gambit_preprocess_page(&$variables){

    $widthClasses = array();
    $widthClasses['main'] = "";
    $widthClasses['column'] = "";
        
    // if both columns are populated
    if(!empty($variables['page']['left_callout']) && !empty($variables['page']['right_callout'])){
        $widthClasses['main'] = "one-half";
        $widthClasses['column'] = "one-fourth";
    // if either column is populated    
    } elseif (!empty($variables['page']['left_callout']) || !empty($variables['page']['right_callout'])){
        $widthClasses['main'] = "three-fourths";
        $widthClasses['column'] = "one-fourth";
    // if no columns are populated    
    } else {
        $widthClasses['main'] = "";
    }
    
    $variables['gambit_widthClasses'] = $widthClasses;
}

 ?>
 
 
 
 
 
 
 
 
 
 
 
 